package A;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import AB.Calculator;

public class CalculatorTest {
	@Test
	public void ShouldCalculateProduct() {
		Calculator Calc=new Calculator();
		int Result = Calc.Multiply(4, 3, 2);
		assertEquals(24, Result, 0);
	}

}