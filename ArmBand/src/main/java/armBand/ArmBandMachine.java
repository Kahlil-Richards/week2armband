package armBand;

import APlib.IArmBandMachine;

public class ArmBandMachine implements IArmBandMachine  {
	
	public float getCost(float price, int quantity){
		return price * quantity;
	}

}