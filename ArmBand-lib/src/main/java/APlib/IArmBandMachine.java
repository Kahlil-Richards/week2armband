package APlib;

public interface IArmBandMachine {
	/**
	 * <h3>getCost <h3/> - html styling
	 * 
	 * @param pice - Cost for an ArmBand
	 * @param quantity - Amount of Armbands
	 * @return
	 */
	
	float getCost(float pice, int quantity);

}
