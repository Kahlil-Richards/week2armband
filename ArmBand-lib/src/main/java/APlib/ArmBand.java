package APlib;

public class ArmBand {
	private int ID;
	private String Colour;
	private float Price;
	
	public ArmBand() {
		super();
		ID = 0;
		Colour = null;
		Price = 0;
	}
	
	public ArmBand(int iD, String colour, float price) {
		super();
		ID = iD;
		Colour = colour;
		Price = price;
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getColour() {
		return Colour;
	}
	public void setColour(String colour) {
		Colour = colour;
	}
	public float getPrice() {
		return Price;
	}
	public void setPrice(float price) {
		Price = price;
	}
	
	
	@Override
	public String toString() {
		return "ArmBand [ID=" + ID + ", Colour=" + Colour + ", Price=" + Price
				+ "]";
	}
	
}
